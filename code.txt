DROP TABLE emergency cascade constraints;

DROP TABLE english_pro cascade constraints;

DROP TABLE student cascade constraints;

DROP TABLE incoming_student cascade constraints; 

DROP TABLE visa cascade constraints;

DROP TABLE bank_details cascade constraints;

DROP TABLE institution cascade constraints;

DROP TABLE ba_agreement cascade constraints;

DROP TABLE module cascade constraints;

DROP TABLE la_agreement cascade constraints;

DROP TABLE course cascade constraints;

DROP TABLE course_module cascade constraints; 

CREATE TABLE emergency(
	emergency_id NUMBER(8) NOT NULL,
    emergency_name   VARCHAR(25) NOT NULL,
	relationship	 VARCHAR(15),
    street_no        NUMBER  (8) NOT NULL,
    town_city	     VARCHAR(30),
	country	         VARCHAR(30),
    postcode         VARCHAR(10) NOT NULL,
    mobile	         NUMBER (14) NOT NULL,
   
    CHECK (street_no >0),
    CONSTRAINT emegency_name CHECK (REGEXP_LIKE(emergency_name, '[[:alpha:]]')),
    CONSTRAINT relationship CHECK (REGEXP_LIKE(relationship, '[[:alpha:]]')),
    CONSTRAINT town_city CHECK (REGEXP_LIKE(town_city, '[[:alpha:]]')),
    CONSTRAINT country CHECK (REGEXP_LIKE(country, '[[:alpha:]]')),
    CONSTRAINT postcode CHECK (postcode = upper (postcode)),
    
    CONSTRAINT	pk_emergency PRIMARY KEY (emergency_id)
     
   
);

CREATE TABLE english_pro(
	english_pro_id   NUMBER(8) NOT NULL,
    english_speaking_country   VARCHAR(5) DEFAULT 'No' NOT NULL,
	english_qualification	   VARCHAR(50),
    first_language	           VARCHAR(30),
    CONSTRAINT first_language CHECK (REGEXP_LIKE(first_language, '[[:alpha:]]')),
    CONSTRAINT	pk_english_pro PRIMARY KEY (english_pro_id)
);

CREATE TABLE student(
	student_id	NUMBER(8) NOT NULL,
	title	    VARCHAR(10) DEFAULT 'Miss' NOT NULL,
	surname	    VARCHAR(25) NOT NULL,
    firstname   VARCHAR(25),
    outbound_inbound VARCHAR(20),
    dob	        DATE        NOT NULL,
	Disability	VARCHAR(05),
    gender      VARCHAR(10) DEFAULT 'Female' NOT NULL,
    email	    VARCHAR(50),
	telephone	NUMBER (10),
    street_no   NUMBER  (8) NOT NULL,
    town_city	VARCHAR(30),
	country	    VARCHAR(30),
    postcode    VARCHAR(10) NOT NULL,
    co_mobile	NUMBER (10) NOT NULL,
	coordinator	VARCHAR (50)NOT NULL,
    co_email    VARCHAR (50),
    arrival_date DATE,
	departure_date	DATE,
	fk1_emergency_id NUMBER(8) NOT NULL,
    CHECK (street_no >0),
    CHECK (postcode = upper (postcode)),
    CHECK (REGEXP_LIKE(Disability, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE(gender, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE(town_city, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE(country, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE(coordinator, '[[:alpha:]]')),
    CONSTRAINT surname CHECK (REGEXP_LIKE(surname, '[[:alpha:]]')),
    CONSTRAINT firstname CHECK (REGEXP_LIKE(firstname, '[[:alpha:]]')),
    CONSTRAINT	pk_student PRIMARY KEY (student_id));

CREATE TABLE incoming_student(
	incstudent_id   NUMBER(8) NOT NULL ,
    student_name VARCHAR(30),
    country_of_birth   VARCHAR(30)  NOT NULL,
	nationality  VARCHAR(25) NOT NULL,
    ethnicity	 VARCHAR(30) NOT NULL,
    fk1_english_pro_id NUMBER(8) NOT NULL,
    fk2_student_id NUMBER(8) NOT NULL,
    CONSTRAINT country_of_birth CHECK (REGEXP_LIKE(country_of_birth, '[[:alpha:]]')),
    CONSTRAINT nationality CHECK (REGEXP_LIKE(nationality, '[[:alpha:]]')),
    CONSTRAINT ethnicity CHECK (REGEXP_LIKE(ethnicity, '[[:alpha:]]')),
    CONSTRAINT	pk_incoming_student PRIMARY KEY (incstudent_id)
    
);

CREATE TABLE visa(
	visa_id   NUMBER(8) ,
    visa_start_date   DATE  NOT NULL,
	visa_expiry_date  DATE,
    status VARCHAR(15) NOT NULL,
    location	 VARCHAR(25) NOT NULL,
    fk1_incstudent_id NUMBER(8) NOT NULL,
    CONSTRAINT location CHECK (REGEXP_LIKE(location, '[[:alpha:]]')),
    CONSTRAINT	pk_visa PRIMARY KEY (visa_id)
);

CREATE TABLE bank_details (
	bank_sortcode   NUMBER(8) NOT NULL,
    bank_name       VARCHAR (20)  NOT NULL,
	account_name    VARCHAR(50)  NOT NULL,
    account_number	NUMBER(8) NOT NULL,
    fk1_student_id  NUMBER(8) NOT NULL,
    CONSTRAINT bank_name CHECK (REGEXP_LIKE(bank_name, '[[:alpha:]]')),
    CONSTRAINT account_name CHECK (REGEXP_LIKE(account_name, '[[:alpha:]]')),
    CONSTRAINT	pk_bank_details PRIMARY KEY (bank_sortcode)
);

--------------------------------------------------------------------------------
CREATE TABLE institution (
	institution_code NUMBER(8) NOT NULL,
    erasmus_code VARCHAR (20),
    institution_name  VARCHAR (50) NOT NULL,
    country     VARCHAR (20) NOT NULL,
    street_no   NUMBER  (8)  NOT NULL,
    postcode    VARCHAR(10)  NOT NULL,
    town_city	VARCHAR(30)  NOT NULL,
    CHECK (street_no >0),
    CHECK (postcode = upper (postcode)),
    CONSTRAINT  institution_name CHECK (REGEXP_LIKE( institution_name, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE( country, '[[:alpha:]]')),
    CHECK (REGEXP_LIKE( town_city, '[[:alpha:]]')),
    
    CONSTRAINT	pk_institution PRIMARY KEY (institution_code)
);

--------------------------------------------------------------------------------

CREATE TABLE ba_agreement (
	ba_agreement_id NUMBER(8) NOT NULL,
    duration        VARCHAR (30)  NOT NULL,
    fk1_institution_code  NUMBER(8) NOT NULL,
    CONSTRAINT	pk_ba_agreement PRIMARY KEY (ba_agreement_id)
);

--------------------------------------------------------------------------------
CREATE TABLE module (
	module_id NUMBER(8) NOT NULL,
    name      VARCHAR (20)  NOT NULL,
    CONSTRAINT  name CHECK (REGEXP_LIKE( name, '[[:alpha:]]')),
    CONSTRAINT	pk_module PRIMARY KEY (module_id)
);

--------------------------------------------------------------------------------

CREATE TABLE la_agreement (
	la_id             NUMBER(8) NOT NULL,
    module_name    VARCHAR(50),
    UK_credit   NUMBER  (8) NOT NULL,
    ECTS        NUMBER  (8) NOT NULL,
    fk1_institution_code  NUMBER(8) NOT NULL,
    fk2_module_id  NUMBER(8) NOT NULL,
    fk3_student_id NUMBER(8) NOT NULL,
    CONSTRAINT	pk_la_agreement PRIMARY KEY (la_id)
);

--------------------------------------------------------------------------------
CREATE TABLE course (
	course_id NUMBER(8) NOT NULL ,
    name      VARCHAR (30)  NOT NULL,
    department VARCHAR(50)  NOT NULL,
    CHECK (REGEXP_LIKE( name, '[[:alpha:]]')),
    CONSTRAINT  department CHECK (REGEXP_LIKE( department, '[[:alpha:]]')),
    CONSTRAINT	pk_course PRIMARY KEY (course_id)
);

--------------------------------------------------------------------------------

CREATE TABLE course_module(
	course_module_id NUMBER(8),	
	module_id	     NUMBER(8),
	course_id	 NUMBER(8),
	PRIMARY KEY (course_module_id),
	FOREIGN KEY (module_id)     REFERENCES module (module_id),
	FOREIGN KEY (course_id) REFERENCES course (course_id) 
);

--------------------------------------------------------------------------------


-- This constraint ensures that the foreign key of table "emergency"
-- correctly references the primary key of table "student"

ALTER TABLE emergency ADD CONSTRAINT fk1_student_id FOREIGN KEY(fk1_student_id) REFERENCES student (student_id);


-- This constraint ensures that the foreign key of table "incoming_student"
-- correctly references the primary key of table "english_pro"

ALTER TABLE incoming_student ADD CONSTRAINT fk1_incoming_student_english FOREIGN KEY(fk1_english_pro_id) REFERENCES english_pro (english_pro_id);


-- This constraint ensures that the foreign key of table "incoming_student"
-- correctly references the primary key of table "english_pro"

ALTER TABLE incoming_student ADD CONSTRAINT fk2_incoming_student FOREIGN KEY(fk2_student_id) REFERENCES student (student_id);


-- This constraint ensures that the foreign key of table "visa"
-- correctly references the primary key of table "incoming_student"

ALTER TABLE visa ADD CONSTRAINT fk1_visa_incomingstudent FOREIGN KEY(fk1_incstudent_id) REFERENCES incoming_student (incstudent_id);


-- This constraint ensures that the foreign key of table "bank_details"
-- correctly references the primary key of table "student"

ALTER TABLE bank_details ADD CONSTRAINT fk1_bank_details_student FOREIGN KEY(fk1_student_id) REFERENCES student (student_id);

-- This constraint ensures that the foreign key of table "ba_agreement"
-- correctly references the primary key of table "institution"

ALTER TABLE ba_agreement ADD CONSTRAINT fk1_ba_agreement_institution FOREIGN KEY(fk1_institution_code) REFERENCES institution (institution_code);


-- This constraint ensures that the foreign key of table "la_agreement"
-- correctly references the primary key of table "institution"

ALTER TABLE la_agreement ADD CONSTRAINT fk1_la_agreement_institution FOREIGN KEY(fk1_institution_code) REFERENCES institution (institution_code);


-- This constraint ensures that the foreign key of table "la_agreement"
-- correctly references the primary key of table "module"

ALTER TABLE la_agreement ADD CONSTRAINT fk2_la_agreement_module FOREIGN KEY(fk2_module_id) REFERENCES module (module_id);


-- This constraint ensures that the foreign key of table "la_agreement"
-- correctly references the primary key of table "student"

ALTER TABLE la_agreement ADD CONSTRAINT fk3_la_agreement_student FOREIGN KEY(fk3_student_id) REFERENCES student (student_id);

